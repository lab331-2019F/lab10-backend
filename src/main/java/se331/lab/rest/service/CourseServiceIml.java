package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseDao;
import se331.lab.rest.dao.LecturerDao;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Slf4j
public class CourseServiceIml implements CourseService{


        @Autowired
    CourseDao courseDao;

        @Override
        @Transactional
        public List<Course> getAllCourse() {
            List<Course> courses = courseDao.getAllCourse();
            return courses;
        }
//    @Override
//    public Lecturer findById(Long lecturerId) {
//        return lecturerDao.findById(lecturerId);
//    }

        @Override
        public Course saveCourse(Course course) {
            return courseDao.saveCourse(course);
        }
    }


