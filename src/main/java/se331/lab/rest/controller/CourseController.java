package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseService;
import se331.lab.rest.service.LecturerService;
@Controller
@Slf4j
public class CourseController {

        @Autowired
        CourseService courseService;

        @GetMapping("/courses")
        public ResponseEntity getAllCourse() {
            return
                    ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(courseService.getAllCourse()));
        }
//    @GetMapping("/lecturers/{id}")
//    public ResponseEntity getLecturerById(@PathVariable("id") Long id) {
//        return ResponseEntity.ok(lecturerService.findById(id));
//    }
        @PostMapping("/courses")
        public ResponseEntity saveCourse(@RequestBody Course course) {
            return ResponseEntity.ok(courseService.saveCourse(course));
        }
    }


